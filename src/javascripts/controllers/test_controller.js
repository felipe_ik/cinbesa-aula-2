(function() {
  angular.module("myApp").controller("RegistrationController", function($scope, $http, $location) {
    $scope.goToTest = function() {
      $location.path("/test")
    };
    $scope.goToAlert = function() {
      $location.path("/alert")
    };
    $scope.submit = function() {
      $scope.submitting = true;
      $http.post("http://localhost/sign_up", { user: { email: $scope.email }})
      .success(function() {
        $location.path("/success");
      }).error(function() {
        $scope.submitting = false;
        $scope.errorMessage = true;
      });
    };
  });
})();
