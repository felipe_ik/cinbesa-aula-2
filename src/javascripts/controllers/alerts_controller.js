(function() {
  angular.module("myApp").controller("AlertsController", function($scope, Alert) {
    $scope.submit = function() {
      Alert.message($scope.message);
    };
  });
})();
