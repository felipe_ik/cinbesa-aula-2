(function() {
  var app = angular.module("webApp", []);

  app.service("Alert", function() {
    return {
      message: function(text) {
        window.alert(text);
      }
    }
  })
})();
