(function() {
  var app = angular.module("routesApp", ["ngRoute"]);

  app.config(function($routeProvider) {
    $routeProvider.
    when("/", {
      templateUrl: 'registration.html',
      controller: 'RegistrationController'
    }).
    when("/test", {
      templateUrl: 'test.html'
    }).
    when("/alerts", {
      templateUrl: 'alert.html',
      controller: 'AlertsController'
    })
  })
})();
