describe('AlertsController', function() {
  var scope, $controller, Alert;

  beforeEach(module("myApp"));

  beforeEach(inject(function(_$controller_, _Alert_) {
    $controller   = _$controller_;
    Alert         = _Alert_;

    scope = { message: "Mensagem" };
    $controller("AlertsController", {$scope: scope});
  }));

  describe("on submit", function() {

    it("alerts message", function() {
      var alert = sinon.mock(Alert)
      alert.expects("message");

      scope.submit();
      alert.verify();
    });
  });
});
