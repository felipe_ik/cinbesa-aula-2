describe('RegistrationController', function() {
  var scope, $controller, $httpBackend, $location;

  beforeEach(module("myApp"));

  beforeEach(inject(function(_$controller_, _$httpBackend_, _$location_) {
    $controller   = _$controller_;
    $httpBackend  = _$httpBackend_;
    $location     = _$location_;

    scope = { email: "email@mail.com" };
    $controller("RegistrationController", {$scope: scope});
  }));

  describe("on submit", function() {
    it("sets submitting to true", function() {
      $httpBackend.expectPOST("http://localhost/sign_up", {user: { email: "email@mail.com" }}).respond(200);

      scope.submit();
      expect(scope.submitting).toBe(true);
    });

    it("submits data to server", function() {
      $httpBackend.expectPOST("http://localhost/sign_up", {user: { email: "email@mail.com" }}).respond();
      scope.submit();
    });

    describe("on success", function() {
      beforeEach(function() {
        $httpBackend.expectPOST("http://localhost/sign_up", {user: { email: "email@mail.com" }}).respond();
      });

      it("path should redirect to /success", function() {
        scope.submit();
        $httpBackend.flush();
        expect($location.url()).toBe("/success");
      });
    })

    describe("on error", function() {
      beforeEach(function() {
        $httpBackend.expectPOST("http://localhost/sign_up", {user: { email: "email@mail.com" }}).respond(500, '');
      });

      it("submittting to false", function() {
        scope.submit();
        $httpBackend.flush();
        expect(scope.submitting).toBe(false);
      });

      it("shows error message", function() {
        scope.submit();
        $httpBackend.flush();
        expect(scope.errorMessage).toBe(true);

      });
    })
  })
});
