describe('AlertService', function() {
  var Alert;

  var showMessage = sinon.spy();

  window.plugins = { toast: {
    showMessage: showMessage
  } };

  beforeEach(module("mobileApp"));

  beforeEach(inject(function(_Alert_) {
    Alert   = _Alert_;
  }));

  describe("message", function() {
    it("alerts message on android", function() {

      Alert.message();
      expect(showMessage.called).toBe(true);
    });
  });
});
