module.exports = function(config){
  config.set({

    basePath : '../',

    files : [
      'src/javascripts/lib/angular.js',
      'src/javascripts/lib/angular-route.js',
      'src/javascripts/lib/angular-resource.js',
      'test/lib/angular-mocks.js',

      'src/javascripts/**/*.js',
      'test/unit/**/*.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine', 'sinon-chai'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-jasmine',
            'karma-sinon-chai'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
